file_name = "strings.txt"
if File.readable?(file_name)
	modified_text = File.read(file_name).gsub(/\.\n\n/, "¿").gsub(/\.\n/, "¡").gsub(/\s+/, " ")
	modified_text = modified_text.gsub(" . ", ". ").gsub(". .", ". ").gsub("¡", ".\n").gsub("¿", ".\n\n")
	
	File.open("modified_file.txt", "w") { |file| file.write(modified_text) }

	puts "The number of 'dis-' words is: #{modified_text.downcase.scan(/^[dis]/).size}"
	puts "The number of '-ing' words is: #{modified_text.downcase.scan(/[ing]$|[ing.]$/).size}"
else
	puts "File not found"
end