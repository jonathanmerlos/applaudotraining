require 'csv'
require 'time'
ENV["TZ"] = "EST"

file_name = "events.csv"
file_content = CSV.read(file_name)
i = 1
converted_dates = []
wrong_dates = []

file_content.each do |desc, date|
	begin
		new_date = Time.parse(date).to_s
		converted_dates << [desc, new_date]
	rescue ArgumentError
		puts "Invalid time at line #{i}"
		wrong_dates << [i, desc, date]
	end
	i += 1
end

CSV.open("converted_dates.csv", "w") do |csv|
  converted_dates.sort_by{|line| line[1]}.each { |line| csv << line }
end

CSV.open("wrong_dates.csv", "w") do |csv|
  wrong_dates.each { |line| csv << line }
end

