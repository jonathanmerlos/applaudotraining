require_relative "week1-2.rb"

def mean(array_to_check)
	if !array_to_check.empty?
    array_to_check.inject(0.0){|sum, n| sum += n}/array_to_check.length
  else "Empty Array" 
  end
end

def median(array_to_check)
  if !array_to_check.empty?
    array_to_check = array_to_check.sort
    median_pos = array_to_check.length / 2
    array_to_check.length % 2 == 1 ? array_to_check[median_pos] : mean(array_to_check[median_pos-1..median_pos])
  else "Empty Array"  
  end
end

def mode(array_to_check)
	if !array_to_check.empty?
		new_histogram = array_to_check.to_histogram
		max_times = new_histogram.values.max
		if max_times > 1
			modes = []
			new_histogram.each_pair do |number, times| 
				if times == max_times 
					modes << number 
				end
			end
		end
		modes ? modes[0...modes.size] : "No mode"
  else "Empty Array"  
  end
end

new_array = [1,2,3, 4,5,9]

puts mean(new_array)
puts median(new_array)
puts mode(new_array)

