class Array
	def to_histogram
		Hash[*group_by{|n| n}.flat_map{|n, f| [n, f.size]}]
	end
end
puts [1, 2, 2, 2, 3, 3].to_histogram